
Docker container for qBittorrent based from [linuxserver/qbittorrent](https://github.com/linuxserver/docker-qbittorrent) adding rcloud support.

The [Qbittorrent](https://www.qbittorrent.org/) project aims to provide an open-source software alternative to µTorrent. qBittorrent is based on the Qt toolkit and libtorrent-rasterbar library.

