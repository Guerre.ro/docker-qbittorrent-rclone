#!/bin/bash
#
# This is the setup script. It will run once on the container first start.
# If you would like to modify the startup script, please modify startup.sh
#
CONTAINER_ALREADY_STARTED="FIRST_RUN"
echo "-- First container startup --"
# make our folders
mkdir -p \
	/config/qBittorrent \
	/config/data
# copy config
[[ ! -e /config/qBittorrent/qBittorrent.conf ]] && \
	cp /defaults/qBittorrent.conf /config/qBittorrent/qBittorrent.conf
# chown download directory if currently not set to qbit
if [[ "$(stat -c '%U' /downloads)" != "qbit" ]]; then
       	chown -R qbit:qbit /downloads
fi
# permissions
chown -v -R qbit:qbit /config
touch $CONTAINER_ALREADY_STARTED
# rclone config create datadir drive config_is_local false
echo -e " $UMASK_SET \n $WEBUI_PORT \n $RCLONE_DRIVE \n $CONTAINER_ALREADY_STARTED   " > /tmp/vars
echo "-- Container startup finished --"
echo -e "\n !!!!!   WARNING   !!!!! \n"
echo -e "Please set up the rclone drive before exiting this shell, "
echo -e "otherwise, the container won't restart. \n"
echo -e "You might also want to modify the rclone mount script at \n"
echo -e "/defaults/startup.sh \n\n"
