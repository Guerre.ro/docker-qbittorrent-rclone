#!/bin/bash
UMASK_SET=${UMASK_SET:-022}
WEBUI_PORT=${WEBUI_PORT:-8080}
RCLONE_DRIVE=${RCLONE_DRIVE:-gcrypt}
CONTAINER_ALREADY_STARTED="FIRST_RUN"
echo $RCLONE_DRIVE
if [ ! -e $CONTAINER_ALREADY_STARTED ]; then
	bash /defaults/setup.sh
else
	echo "-- Not first container startup --"
	umask "$UMASK_SET"
#	rclone mount $RCLONE_DRIVE: /data
#		--allow-other --timeout 1h \
#		--allow-non-empty --cache-db-purge \
#		--buffer-size 32M --use-mmap \
#		--dir-cache-time 72h \
#		--drive-chunk-size 16M \
#		--vfs-cache-mode minimal \
#		--vfs-read-chunk-size 128M \
#		--vfs-read-chunk-size-limit 1G && \
		exec setuidgid qbit /usr/bin/qbittorrent-nox --webui-port="${WEBUI_PORT}"
fi
